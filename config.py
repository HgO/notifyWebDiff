import os, json
from getpass import getpass

from collections import MutableMapping, Mapping

def deep_update(d, u):
    for k, v in u.items():
        if isinstance(v, Mapping):
            d[k] = deep_update(d.get(k, {}), v)
        else:
            d[k] = v
    return d

def deep_apply(d, default = {}):
    d = d.copy()
    for k, v in d.items():
        if k in default and callable(default[k]):
            d[k] = default[k](v)
        elif callable(v):
            d[k] = v()
        elif isinstance(v, Mapping):
            d[k] = deep_apply(v, default.get(k, {}))
    return d


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, set):
            return list(o)
        
        super().default(o)

class TextInput:
    def __init__(self, name, description, default = None, type = None):
        self.name = name
        self.description = description
        self.default = default
        self.type = type
    
    def input(self, prompt):
        return input(prompt)
    
    def read(self):
        description = self.description
        if self.default:
            description += ' [%s]' % self.default
        description += ': '
        
        value = None
        while value is None:
            value = self.input(description).strip()
            
            if not len(value):
                value = self.default
            elif self.type:
                value = self.type(value)
                
        self.value = value
        
        return value

class PasswordInput(TextInput):
    def input(self, prompt):
        return getpass(prompt)

class ConfigManager(MutableMapping):
    def __init__(self, filename):
        self.filename = filename
        self.config = {}
        
    def __getitem__(self, key):
        return self.config[key]
    
    def __setitem__(self, key, value):
        self.config[key] = value
    
    def __delitem__(self, key):
        del self.config[key]
        
    def __iter__(self):
        return iter(self.config)
    
    def __len__(self):
        return len(self.config)
    
    
    def read(self, default = {}, inputs = [], autosave = False):
        self.config = deep_apply(default)
        
        try:
            with open(self.filename, 'r') as ifs:
                config = deep_apply(json.load(ifs), default)
                
                deep_update(self.config, config)
        except FileNotFoundError as e:
            pass
        
        if autosave:
            self.write()
        
        for i, x in enumerate(inputs):
            keys = x.name.split('.')
            
            config = self.config
            for key in keys[:-1]:
                if key not in config:
                    config[key] = {}
                config = config[key]
            key = keys[-1]
            
            if key not in config:
                config[key] = x.read()
            
                self.write()
            else:
                x.value = config[key]
            
            for y in inputs[i+1:]:
                if x.name == y.default:
                    y.default = x.value
            
        return self.config
    
    def write(self):
        try:
            os.makedirs(os.path.dirname(self.filename))
        except (FileExistsError, FileNotFoundError):
            pass
        
        with open(self.filename, 'w') as ofs:
            json.dump(self.config, ofs, 
                indent=4, 
                ensure_ascii=False, 
                sort_keys=True,
                cls=JSONEncoder
            )
    
    def update(self, d):
        deep_update(self.config, { k : v for k, v in d.items() if v is not None })
