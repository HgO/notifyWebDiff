import asyncio
import aiohttp
import async_timeout

import os
import sys
import html
import hashlib
import shutil
import math
import traceback
import re
import string

import cchardet as chardet
from unidecode import unidecode

import bleach

from urllib.parse import urlparse, quote, unquote

from bs4 import BeautifulSoup
from datetime import datetime

from mailer import Mailer
from differ import Differ
from config import ConfigManager

from singleton import Singleton

def normalize(s):
    valid_chars = '.-_() %s%s' % (string.ascii_letters, string.digits)
    replace_chars = '- '
    
    s = unidecode(s)
    s = ''.join(' ' if c in replace_chars else c for c in s if c in valid_chars)
    s = re.sub(' +', ' ', s).lower().replace(' ', '_')
    
    return s

def abspath(path, start = __file__):
    return os.path.realpath(
        os.path.join(
            os.path.dirname(os.path.normpath(start)), 
            path
        )
    )


class HTMLParser(metaclass=Singleton):
    def __init__(self):
        self.mailto_regex = re.compile(r'<(mailto:.+?)>')
        
    def parse(self, text):
        def linkify_mailto(match):
            mail = urlparse(match.group(1))
            mail = (mail.path + '?' + mail.query) if mail.query else mail.path
            
            return '<a href="mailto:' + mail + '">' + unquote(mail) + '</a>'
            
        text = self.replace(self.mailto_regex, linkify_mailto, text)
        
        text = bleach.linkify(text, parse_email=True)
        
        text = ''.join('<p>' + par.replace('\n', '<br/>') + '</p>' for par in text.split('\n\n'))
        
        return text
    
    def replace(self, regex, fun, text):
        offset = 0
        for match in regex.finditer(text):
            i, j = (n + offset for n in match.span())
            
            repl = fun(match)
            offset += len(repl) - len(match.group(0))
            
            text = text[:i] + repl + text[j:]
    
        return text

class Downloadable:
    DEFAULT_TIMEOUT = 180
    
    def __init__(self, url):
        self.url = url
        self.content = None
    
    @asyncio.coroutine
    def download(self, timeout = DEFAULT_TIMEOUT):
        with aiohttp.ClientSession() as session:
            try:
                with async_timeout.timeout(timeout):
                    response = yield from session.get(self.url)
                    
                    try:
                        if response.status != 200:
                            raise RuntimeError("Error " + str(response.status) + " " + self.url)
                        
                        return (yield from response.read())
                    except Exception as e:
                        # .close() on exception.
                        response.close()
                        raise e
                    finally:
                        # .release() otherwise to return connection into free connection pool.
                        # It's ok to release closed response:
                        # https://github.com/KeepSafe/aiohttp/blob/master/aiohttp/client_reqrep.py#L664
                        yield from response.release()
            except (asyncio.TimeoutError, aiohttp.client_exceptions.ClientOSError):
                raise RuntimeError("Cannot connect to the page " + self.url)
    
    @asyncio.coroutine
    def fetch(self, timeout = DEFAULT_TIMEOUT):
        self.content = yield from self.download(timeout)
        print('Downloaded file at', self.url, '(%d bytes)' % len(self.content))
        
class Attachment(Downloadable):
    def __init__(self, url, filename = None):
        super().__init__(url)
        
        self.filename = normalize(os.path.basename(url)) if filename is None else filename
    
    @asyncio.coroutine
    def fetch(self, timeout = Downloadable.DEFAULT_TIMEOUT):
        try:
            yield from super().fetch(timeout)
        except RuntimeError as e:
            print(e)

class Page(Downloadable):
    def __init__(self, name, config):
        self.path = os.path.join(abspath(config['cache_dir']), normalize(name))
        
        self.config = ConfigManager(os.path.join(self.path, 'page.json'))
        self.config.read(default={ 
            'admins' : set, 
            'users' : set, 
            'attachments' : set, 
            'logs' : dict, 
            'history' : dict,
            'name' : name
        })
        
        if 'url' in self.config:
            super().__init__(self.config['url'])
        
        self.gconfig = config
        self.mailer = Mailer(config['mail'])
        
    @asyncio.coroutine
    def fetch(self):
        print("Processing", self.config['name'])
        
        try:
            self.content = yield from self.download()
            self.content = self.content.decode(chardet.detect(self.content)['encoding'])
    
            soup = BeautifulSoup(self.content, "html.parser")
            
            if 'select' in self.config and self.config['select']:
                page = soup.select(self.config['select'])
            
                if not len(page):
                    raise RuntimeError("Cannot parse the page " + self.config['url'])
                
                soup = page[0]
        
            [ s.extract() for s in soup("script") ]
            [ s.extract() for s in soup("head") ]
            [ s.extract() for s in soup("form") ]
            [ s.extract() for s in soup("nav") ]
                
            text = str(soup).strip()
            htext = hashlib.sha1(text.encode('utf-8')).hexdigest()
    
            if 'hash' not in self.config or htext != self.config['hash']:
                date = datetime.now().isoformat()
        
                if htext not in self.config['history']:
                    message_diff = {}
                    attachments = []
                    
                    diff = None
                    try:
                        with open(os.path.join(self.path, 'index.html'), 'r') as ifs:
                            diff = Differ.diff(ifs.read(), text)
                    except (IOError, KeyError):
                        pass
                    
                    if diff is not None:
                        added_text = '\n'.join(line[1:].strip() for i, line in diff if line.startswith('+'))
                        
                        if len(added_text):
                            coroutines = []
                            for attachment in self.find_attachments(added_text):
                                if attachment.url not in self.config['attachments']:
                                    coroutines.append(attachment.fetch(timeout = 60))
                                    attachments.append(attachment)
                                    
                            yield from asyncio.gather(*coroutines)
                            
                            attachments = [ a for a in attachments if a.content is not None ]
                            for attachment in attachments:
                                self.config['attachments'].add(attachment.url)

                        message_diff = { 
                            content_type : Differ.show(diff, content_type) 
                                for content_type in ('html', 'plain', 'terminal')
                        }
                
                    with open(os.path.join(self.path, 'index.html'), "w") as ofs:
                        ofs.write(text)
                    
                    subject, body = self.open_message(**self.gconfig['messages']['admin'])
                    message = { 
                        'plain' : body, 
                        'terminal' : body,
                        'html' : HTMLParser().parse(body)
                    }
                    
                    for content_type, content in message.items():
                        if content_type in message_diff:
                            message[content_type] = content.format(diff = message_diff[content_type])
                        else:
                            message[content_type] = content.format(diff = '')
                    
                    print(message['terminal'])
                    
                    self.mailer.sendmail(
                        self.config['admins'], 
                        subject, 
                        message, 
                        reply = self.gconfig['mail']['notifier'], 
                        attachments = attachments
                    )
            
                self.config['modified'] = date
                self.config['hash'] = htext
                
                self.config['history'][htext] = date
            elif 'modified' in self.config:
                date = datetime.strptime(self.config['modified'], "%Y-%m-%dT%H:%M:%S.%f")
                now = datetime.now()
                    
                days = (now - date).days
        
                if (now.month >= 7 and now.month <= 9 and days > 75) or ((now.month >= 10 or now.month <= 6) and days > 45):
                    raise RuntimeError("The following page hasn't been modified for " + str(days) + " days: " + self.config['url'])
            
            if 'error' in self.config:
                del self.config['error']
                for error in self.config['logs'].values():
                    error['counter'] = 0
            
            self.config.write()
        except RuntimeError as e:
            self.error(str(e))
            self.config.write()
        except Exception:
            print(traceback.format_exc(), file=sys.stderr)
    
    def find_attachments(self, html):
        soup = BeautifulSoup(html, 'html.parser')
        
        parsed_url = urlparse(self.config['url'])
        
        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_url)
        base = domain + os.path.dirname(parsed_url.path)
        
        for a in soup.find_all('a', href=True):
            url = a['href']
            
            if not url.endswith('.pdf'):
                continue
            
            if url.startswith('/'):
                url = domain + url[1:]
            elif not url.startswith('http'):
                url = os.path.join(base, url)
            
            yield Attachment(url)
            
    def open_message(self, subject, body):
        subject = subject.format(
            title = self.gconfig['title'],
            page = self.config['name']
        )
        
        with open(abspath(body), encoding='utf-8') as ifs:
            message = ifs.read().format(
                url = self.config['url'],
                page = self.config['name'],
                normalized_page = quote(self.config['name']),
                mail = self.gconfig['mail'],
                diff = '{diff}',
                error = '{error}'
            )
        
        return subject, message
    
    def error(self, message):
        logs = self.config['logs']
        
        if message not in logs:
            logs[message] = {
                'counter' : 0,
            }
        
        logs[message]['date'] = datetime.now().isoformat()
        logs[message]['counter'] += 1
        
        self.config['error'] = message
        
        c = logs[message]['counter']
        base = 12
        output = sys.stdout
        if c >= base:
            trigger = (1 << int(math.log2(c // base))) * base
            if c == trigger:
                subject, body = self.open_message(**self.gconfig['messages']['error'])
                message = { 'plain' : body.format(error = self.config['error']) }
                
                self.mailer.sendmail(self.gconfig['mail']['postmaster'], subject, message) 
                
        print(message, file=output)
    
    def notify(self, mail = None, admin = False):
        if mail is None:
            mails = self.config['users']
            if admin:
                mails |= self.config['admins']
        else:
            mails = { mail }
        
        if not len(mails):
            return
        
        subject, body = self.open_message(**self.gconfig['messages']['user'])
        message = { 
            'plain' : body, 
            'html' : HTMLParser().parse(body) 
        }
        
        self.mailer.sendmail(mails, subject, message, self.gconfig['mail']['postmaster'])
    
    def create(self, url, select = None, users = [], admins = []):
        if os.path.exists(self.path):
            raise FileExistsError("Cannot create the page, directory exists: " + self.path)
        
        self.enable()
        
        self.config.update({
            'url' : url, 
            'users' : set(users), 
            'admins' : set(admins),
            'select' : select
        })
        self.config.write()
        
        self.url = url
        
    def enable(self):
        if self.config['name'] not in self.gconfig['pages']:
            self.gconfig['pages'].add(self.config['name'])
            self.gconfig.write()
        
    def disable(self):
        try:
            self.gconfig['pages'].remove(self.config['name'])
            self.gconfig.write()
        except KeyError:
            pass
        
    def enabled(self):
        return self.config['name'] in self.gconfig['pages']
    
    def subscribe(self, mail, admin = False):
        if mail not in self.config['users']:
            self.config['users'].add(mail)
            self.config.write()
        
        if admin:
            self.manage(mail)
        
    def manage(self, mail):
        if mail not in self.config['admins']:
            self.config['admins'].add(mail)
            self.config.write()
    
    def unsubscribe(self, mail, admin = False):
        try:
            self.config['users'].remove(mail)
            self.config.write()
        except KeyError:
            pass
        
        if admin:
            self.unmanage(mail)
    
    def unmanage(self, mail):
        try:
            self.config['admins'].remove(mail)
            self.config.write()
        except KeyError:
            pass
    
    def edit_mail(self, mail, new):
        for mailing in ('users', 'admins'):
            try:
                self.config[mailing].remove(mail)
                self.config[mailing].add(new)
                
                self.config.write()
            except KeyError:
                pass
    
    def edit(self, new = None, url = None, select = None, mail = None):
        if not os.path.exists(self.path):
            raise FileNotFoundError("Cannot edit the page, directory doesn't exist: " + self.path)
        
        if mail:
            self.edit_mail(mail, new)
            return
        
        self.config.update({
            'url' : url, 
            'select' : select
        })
        self.config.write()
        
        if new:
            self.move(new)
        
    def move(self, name):
        path = os.path.join(abspath(self.gconfig['cache_dir']), normalize(name))
        
        if path == self.path:
            return
        
        if os.path.exists(path):
            raise FileExistsError("Cannot move the page, directory exists: " + path)
        
        os.rename(self.path, path)
        
        p = Page(name, self.gconfig)
        p.config['name'] = name
        
        if self.enabled():
            self.disable()
            p.enable()
        
        for attribute in self.__dict__:
            self.__dict__[attribute] = p.__dict__[attribute]
        
        self.config.write()
            
    def remove(self):
        shutil.rmtree(self.path)
        self.disable()
