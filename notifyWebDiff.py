#!/usr/bin/python3
# -*- encoding: utf-8 -*-

import argparse
import sys

import io
import re

import asyncio

from config import ConfigManager, TextInput, PasswordInput
from page import Page, normalize, abspath


def find_map(fun, l):
    try:
        return next(x for i, x in enumerate(l) if fun(x))
    except StopIteration:
        return None


class ServiceManager:
    def __init__(self, config):
        self.config = ConfigManager(config)
        self.config.read(
            default = { 
                'pages' : set, 
                'messages' : { 
                    'user' : { 
                        'subject' : '{title} - {page}',
                        'body' : 'templates/message-user.txt'
                    },
                    'admin' : {
                        'subject' : '[admin] {title} - {page}',
                        'body' : 'templates/message-admin.txt'
                    },
                    'error' : {
                        'subject' : '[error] {title} - {page}',
                        'body' : 'templates/message-error.txt'
                    }
                }
            }, inputs = [
                TextInput('cache_dir', 'Le dossier de cache', 'cache'),
                TextInput('mail.postmaster', 'Le mail de contact en cas de problèmes (lien cassé, désinscription, etc.)'),
                TextInput('mail.from', "Le mail d'envoi des mails", 'mail.postmaster'),
                TextInput('mail.notifier', 'Le mail pour envoyer les notifications aux abonné·es'),
                TextInput('mail.host', 'Le serveur de mail SMTP', 'localhost'),
                TextInput('mail.port', 'Le port du serveur de mail', 465, int),
                TextInput('mail.login', 'Le login pour se connecter au serveur de mail', ''),
                PasswordInput('mail.password', 'Le mot de passe pour se connecter au serveur de mail', ''),
                TextInput('title', 'Le titre du mail à afficher quand une modification est notifiée', 'Web Notification')
            ], autosave = True
        )
        
        self.cache_dir = self.config['cache_dir']
        if not self.cache_dir.startswith('/'):
            self.cache_dir = abspath(self.cache_dir)
    
    def create(self, page, url, select = None, admins = [], users = []):
        Page(page, self.config).create(url, select, admins, users)
        
    def subscribe(self, mail, page = None, admin = False):
        if page is None:
            for p in self.config['pages']:
                self.subscribe(mail, p, admin)
        else:
            Page(page, self.config).subscribe(mail, admin)
            
    def manage(self, mail, page = None):
        if page is None:
            for p in self.config['pages']:
                self.manage(mail, p)
        else:
            Page(page, self.config).manage(mail)
    
    def edit(self, page = None, mail = None, new = None, url = None, select = None):
        if mail:
            if not page:
                for p in self.config['pages']:
                    self.edit(p, mail, new)
            else:
                Page(page, self.config).edit_mail(mail, new)
        
        elif page:
            Page(page, self.config).edit(new, url = url, select = select)
            
    
    def rm(self, page):
        Page(page, self.config).remove()
    
    def unsubscribe(self, mail, page = None, admin = False):
        if page is None:
            for p in self.config['pages']:
                self.unsubscribe(mail, p, admin)
        else:
            Page(page, self.config).unsubscribe(mail, admin)
            
    def unmanage(self, mail, page = None):
        if page is None:
            for p in self.config['pages']:
                self.unmanage(mail, p)
        else:
            Page(page, self.config).unmanage(mail)
    
    def disable(self, page):
        Page(page, self.config).disable()
            
    def enable(self, page):
        Page(page, self.config).enable()
    
    def fetch(self, page = None):
        pages = self.config['pages'] if page is None else [ page ]
        
        loop = asyncio.get_event_loop()
        loop.run_until_complete(
            asyncio.gather(
                *(Page(p, self.config).fetch() for p in pages)
            )
        )
        loop.close()
    
    def notify(self, page, mail = None, admin = False):
        Page(page, self.config).notify(mail, admin)
    
    def pipe(self, sender = None, page = None, mail = None, admin = False):
        services = [
            'notify',
            'unsubscribe', 
            'subscribe', 
            'enable',
            'disable',
            'unmanage',
            'manage',
            'invite'
        ]
        
        service = None
        
        if page is None:
            regex_mail = re.compile(r'[^<\s]+@[^>\s]+')
            
            with io.open(0, encoding='utf-8') as stdin:
                for line in stdin:
                    if line.startswith('Subject:'):
                        line = line.lower()
                        service = find_map(lambda service: service in line, services)
                        
                        if not service:
                            break
                        
                        _, _, args = line.lower().partition(service)
                        args = args.strip()
                        
                        if not len(args):
                            break
                        
                        if '@' in args:
                            mail, page = args.split(maxsplit=1)
                        else:
                            page = args
                            mail = None
                        
                    elif line.startswith('From:'):
                        match = regex_mail.search(line[5:])
                        if match:
                            mail_from = match.group().strip()
                            if sender and sender != mail_from:
                                print(sender, mail_from)
                                raise RuntimeError('Sender mismatch')
                            else:
                                sender = mail_from
                    
                    if sender and page:
                        break
            
            if not service:
                raise RuntimeError('You must choose among one of these services:\n\t' + ', '.join(services))
            
            if not sender:
                raise RuntimeError('You must indicate the sender')
            
            if not page:
                raise RuntimeError('You must provide the name of the page')
            
        if service == 'manage' or service == 'invite':
            if not mail:
                raise RuntimeError("You must provide the user's mail")
            
        if service == 'unmanage':
            if mail and mail != sender:
                raise RuntimeError('You cannot remove the admin rights of another user')
        
        page = normalize(page)
        
        if service in ('notify', 'subscribe', 'manage', 'unmanage'):
            if not find_map(lambda p: page == normalize(p), self.config['pages']):
                raise RuntimeError('The page is not enabled')
            
        p = Page(page, self.config)
        
        if sender and sender not in p.config['admins']:
            if service in ('notify', 'invite', 'disable', 'enable', 'manage', 'unmanage') \
                    or (mail and mail != sender and service in ('subscribe', 'unsubscribe')):
                raise RuntimeError('You must be an admin of the page "%s"' % p.config['name'])
        
        if not mail:
            mail = sender
        
        if service == 'notify':
            p.notify(mail, admin)
        elif service == 'subscribe' or service == 'invite':
            p.subscribe(mail)
        elif service == 'unsubscribe':
            p.unsubscribe(mail)
        elif service == 'disable':
            p.disable()
        elif service == 'enable':
            p.enable()
        elif service == 'manage':
            p.manage(mail)
        elif service == 'unmanage':
            p.unmanage(mail)

class ArgumentParser:
    def __init__(self):
        parser = argparse.ArgumentParser()
    
        parser.add_argument('service', choices=[ 
            'subscribe', 
            'unsubscribe',
            'manage',
            'unmanage',
            'create',
            'rm',
            'edit', 
            'notify', 
            'fetch', 
            'enable', 
            'disable',
            'pipe'
        ])
        
        parser.add_argument('-p', '--page')
        parser.add_argument('-m', '--mail')
        parser.add_argument('-u', '--url')
        parser.add_argument('-n', '--new')
        parser.add_argument('-a', '--admin', action='store_true')
        parser.add_argument('--admins', nargs='*')
        parser.add_argument('--users', nargs='*')
        parser.add_argument('-s', '--select')
        parser.add_argument('--sender')
        
        parser.add_argument('-c', '--config', default='config.json')
        
        self.parser = parser
        
        self.args = parser.parse_args()
    
    def validate(self):
        if self.args.service not in ('pipe', 'fetch', 'edit'):
            if not self.args.page:
                raise RuntimeError('You must provide the name of the page')
        
        if self.args.service in ('subscribe', 'unsubscribe', 'manage', 'unmanage'):
            if not self.args.mail:
                raise RuntimeError("You must provide the user's e-mail")
        
        if self.args.service == 'edit':
            if not (self.args.page or self.args.mail):
                raise RuntimeError("You must either provide the name of the page or the user's mail to edit")
            
            if not self.args.new:
                if self.args.mail:
                    raise RuntimeError("You must provide the new user's e-mail")
                
                if self.args.page:
                    if not (self.args.url or self.args.new or self.args.select):
                        raise RuntimeError("You must modify something")
                
        
        if self.args.service == 'create':
            if not self.args.url:
                raise RuntimeError('You must provide the url of the page')
        
        if self.args.service in ('create', 'edit') and self.args.page:
            if self.args.url and not self.args.url.startswith('http'):
                raise RuntimeError('The url must start with http(s)')
        
        return self.args
    
    def parse(self):
        try:
            self.validate()
        except RuntimeError as e:
            self.parser.print_help()
            print('Error:', str(e), file=sys.stderr)
            return
        
        service_manager = ServiceManager(self.args.config)
        
        service_name = self.args.service
        
        service = getattr(service_manager, service_name)
        
        return service(**{ arg : val 
            for arg, val in self.args.__dict__.items() 
                if val is not None and arg in service.__code__.co_varnames[:service.__code__.co_argcount] })
    
if __name__ == '__main__':
    parser = ArgumentParser()
    parser.parse()
