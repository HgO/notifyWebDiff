import smtplib
import ssl

from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import encoders
from email.utils import formatdate, make_msgid

from singleton import Singleton

class Mailer(metaclass=Singleton):
    def __init__(self, config, starttls = None):
        self.config = config
        self.ssl_context = ssl.create_default_context()
        
        if starttls is None:
            self.starttls = self.config['port'] == 587
        else:
            self.starttls = starttls
        
    def connect(self):
        host = self.config['host']
        port = self.config['port']
        
        if self.starttls:
            smtp = smtplib.SMTP(host, port)
            smtp.starttls(context=self.ssl_context)
        else:
            smtp = smtplib.SMTP_SSL(host, port)
        
        login = self.config['login']
        password = self.config['password']
        
        if login and password:
            smtp.login(login, password)
            
        return smtp
    
    def sendmail(self, recipients, subject, body = {}, reply = None, unsubscribe = None, attachments = []):
        if not len(recipients):
            raise ValueError("Mail must be sent to at least one recipient.")
        
        if isinstance(recipients, list):
            recipients = set(recipients)
        elif isinstance(recipients, str):
            recipients = { recipients }
        
        contents = []
        for content_type, content in body.items():
            if content_type in ('html', 'plain'):
                contents.append(MIMEText(content, content_type, _charset='utf-8'))
        
        if not len(contents):
            raise ValueError("Mail must contain at least one body type (html or plain)")
        
        if len(contents) > 1:
            msg = MIMEMultipart("alternative")
            
            for content in contents:
                msg.attach(content)
        else:
            msg = contents[0]
        
        if len(attachments):
            part = msg
            msg = MIMEMultipart("mixed")
            msg.attach(part)
            
            for attachment in attachments:
                part = MIMEBase("application", "octet-stream")
                part.set_payload(attachment.content)
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment; filename="%s"' % attachment.filename)
                
                msg.attach(part)
        
        sender = self.config['from']
        
        msg['From'] = sender
        msg['Subject'] = subject
        msg["Date"] = formatdate(localtime=True)
        msg["Message-ID"] = make_msgid(domain=sender.split('@')[-1])
        
        if unsubscribe:
            msg["List-Unsubscribe"] = "<mailto:%s>" % unsubscribe
        
        if reply:
            msg['Reply-To'] = reply
        
        with self.connect() as smtp:
            for recipient in recipients:
                msg['To'] = recipient
                smtp.sendmail(sender, recipient, msg.as_string())
                del msg['To']
