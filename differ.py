import difflib
import html

try:
    from colorama import Fore, Back, Style, init
    init()
except ImportError:  # fallback so that the imported classes always exist
    class ColorFallback():
        __getattr__ = lambda self, name: ''
    Fore = Back = Style = ColorFallback()


class Painter:
    def paint(self, text, color):
        raise NotImplementedError()
    
class HTMLPainter(Painter):
    def paint(self, text, color):
        return '<span style="color:%s">%s</span>' % (color, text)

class TerminalPainter(Painter):
    def paint(self, text, color):
        return getattr(Fore, color.upper()) + text + Fore.RESET

class Differ:
    painters = {
        'html' : HTMLPainter,
        'terminal' : TerminalPainter
    }
    
    colors = { 
        '+' : 'green',
        '-' : 'red',
        '?' : 'blue'
    }
    
    def diff(a, b):
        a, b = map(lambda s: s.strip(), (a, b))
        
        d = difflib.Differ()
        lines = list(d.compare(a.splitlines(1), b.splitlines(1)))
        
        diff = []
        for i, line in enumerate(lines):
            if line[0] in '+-?':
                diff.append((i, line))
            
            elif i+1 < len(lines) and lines[i+1][0] in '+-?':
                if len(diff) > 0:
                    j, last = diff[-1]
                    
                    if i > j+1:
                        diff.append((i, '==='))
                
                diff.append((i, line))
            
            elif len(diff) > 0:
                j, last = diff[-1]
                
                if i == j+1:
                    if last[0] in '+-?':
                        diff.append((i, line))
        
        return [ (i, line.rstrip()) for i, line in diff if len(line[1:].strip()) ]

    def colorize(diff, painter):
        for i, line in diff:
            
            #if len(line) == 0:
                #return line
            
            try:
                line = painter.paint(line, Differ.colors[line[0]])
            except KeyError:
                pass
                
            yield (i, line)
            
    def show(diff, output = 'html', color = True):
        if output == 'html':
            diff = [ (i, html.escape(line)) for i, line in diff ]
        
        if color and output in Differ.painters:
            diff = Differ.colorize(diff, Differ.painters[output]())
        
        if output == 'plain':
            diff = ((i, '> ' + line) for i, line in diff)
        
        text = '\n'.join(line for i, line in diff)
        
        if output == 'html':
            text = '<blockquote type="cite"><pre>' + text + '</pre></blockquote>'
        
        return text
