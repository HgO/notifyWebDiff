## Prérequis

Vous devez avoir python3.4 ou une version ultérieure pour pouvoir lancer ce script.

La liste des dépendances est la suivante:
- bs4
- bleach
- cchardet
- aiohttp
- async_timeout
- unidecode

**Note:** Pour bleach et html5lib, un bug existe pour la version 1.4.1 : cela produit une boucle infinie... Je n'ai pas rencontré de bug pour la version 1.4.2. Par contre, la version 2.0 de bleach ne semble plus être compatible avec le script.

**Optionnel:** Si vous voulez que le script affiche le diff des pages en couleur sur le terminal, vous devrez installer la librairie `colorama`

## Configuration

Lors de la première utilisation, une série de paramètres vous sera demandée. Entre autres, il vous sera demandé d'indiquer les paramètres pour se connecter à un serveur SMTP afin d'envoyer les notifications par e-mail.

### Crontab

Le script est prévu pour être utilisé comme une tâche cron. Un exemple d'utilisation est le suivant:
```
0 08-20 * * 1-5 /path/to/the/script/notifyWebDiff.py fetch -c /path/to/the/config/file.json > /dev/null
```
Chaque jour de la semaine, et toutes les heures de 8h à 20h, le script va vérifier si des changements ont eu lieu sur les pages à surveiller.

### Commandes par mail

Pour avoir la possibilité d'envoyer des commandes par mail, il vous faudra configurer votre serveur de mail.

Si vous utilisez Postfix, vous pouvez suivre [ce tutoriel](https://thecodingmachine.io/triggering-a-php-script-when-your-postfix-server-receives-a-mail) (en anglais), dont les étapes principales sont reprises ici :

1. Modifiez le fichier /etc/postfix/master.cf pour y rajouter la ligne:
```
notifyWebDiff     unix  -       n       n       -       -       pipe
  flags=F user=<user> argv=/path/to/the/script pipe --sender ${sender} -c /path/to/the/confiq/file.json [-a]
```
avec `user` l'utilisateur qui exécutera le script (pas besoin d'être root), et `-a` l'option indiquant si les notifications doivent également être envoyées aux administrateur·ices (voir [Envoyer des commandes par mail](#envoyer-des-commandes-par-mail)).
2. Créez le fichier /etc/postfix/access avec comme contenu:
```
<notifier> FILTER notifyWebDiff:dummy
```
avec `notifier` l'adresse e-mail qui recevra les commandes, et que vous avez indiqué dans le fichier de configuration.
3. Exécutez la commande `postmap /etc/postfix/access`.
4. Dans le fichier /etc/postfix/main.cf, rajoutez au début de la directive `smtpd_recipient_restrictions`: 
```
check_recipient_access hash:/etc/postfix/access
```
Si la directive n'existe pas, ajouter la ligne suivante:
```
smtpd_recipient_restrictions =
        check_recipient_access hash:/etc/postfix/access, 
        permit_mynetworks, 
        reject_unauth_destination
```
5. Enfin, exécutez la commande `postfix reload`


## Utilisation

De manière générale, le script s'utilise comme suit:
```
./notifyWebDiff <command> <args> [-c <config file>]
```

La liste des commandes disponibles est la suivante:

- `create -p <page> -u <url> [-s <select>] [--admins <admins' mails>] [--users <users' mails>]`
- `edit -p <page> [-n <new page>] [-u <new url>] [-s <new select>]`
- `edit -m <mail> -n <new mail> [-p <page>]`
- `rm -p <page>`
- `enable -p <page>`
- `disable -p <page>`
- `fetch [-p <page>]`
- `notify -p <page> [-m <mail>]`
- `subscribe -m <mail> -p <page> [-a]`
- `unsubscribe -m <mail> -p <page> [-a]`
- `manage -m <mail> -p <page>`
- `unmanage -m <mail> -p <page>`
- `pipe --sender <mail> [-a]`

Elles seront détaillées dans la suite de ce document.

### Créer une page

Pour créer une page à surveiller, lancez la commande:
```
./notifyWebDiff create -p <page> -u <url> [-s <select>] [--admins <admins' mails>] [--users <users' mails>]
```

L'option `-s` permet de sélectionner un élement de la page.

Les options `--admins` et `--users` permettent d'indiquer la liste des administrateur·ices et abonné·es de la page, respectivement.

### Modifier une page

Vous pouvez modifier une page en exécutant la commande suivante:
```
./notifyWebDiff edit -p <page> [-n <new page>] [-u <new url>] [-s <new select>]
```

### Modifier l'e-mail d'un utilisateur

Il est également possible de modifier l'adresse e-mail d'un utilisateur:
```
./notifyWebDiff edit -m <mail> -n <new mail> [-p <page>]
```

Si vous ne précisez pas la page sur laquelle appliquer ce changement, l'adresse e-mail sera modifiée pour toutes les pages **activées**.

### Supprimer une page

Pour supprimer une page, il suffit d'exécuter la commande:
```
./notifyWebDiff rm -p <page>
```

**Attention:** Ces changements sont définitifs, et à l'heure actuelle aucune confirmation ne vous sera demandée !!

### (Dés)activer une page

Le script consulte une liste des pages à surveiller. Vous pouvez ajouter une page à cette liste avec la commande:
```
./notifyWebDiff enable -p <page>
```

Vous pouvez à tout moment arrêter de surveiller une page en utilisant la commande:
```
./notifyWebDiff disable -p <page>
```

### Vérifier les changements

C'est le cœur du programme : pour chaque page activée, le script va récupérer la page web correspondante et vérifier si celle-ci diffère de la version gardée en cache.

Pour ce faire, vous devez lancer la commande suivante:
```
./notifyWebDiff fetch [-p <page>]
```

Si vous spécifiez une page, le script ne récupérera que cette page-là (pratique pour les tests).

Si des changements sont détectés, une notification par e-mail, contenant un diff de la page, sera envoyée aux administrateur·ices de la page.

### Notifier des changements

Lorsque vous considérez que le changement n'était pas un faux-positif, vous pouvez décider d'envoyer une notification par e-mail aux abonné·es de la page:
```
./notifyWebDiff notify -p <page> [-m <mail>] [-a]
```

Si l'option `-a` est spécifiée, les administrateur·ices de la page recevront également la notification.

L'option `-m` permet de dire au programme de n'envoyer la notification qu'à l'adresse e-mail donnée (pratique pour les tests).

### Ajouter / supprimer des abonné·es

Pour abonner une personne à une page, vous pouvez exécuter la commande:
```
./notifyWebDiff subscribe -m <mail> -p <page> [-a]
```

L'option `-a` permet de dire au programme si l'adresse e-mail doit également être ajoutée à la liste des administrateur·ices ou non.

Vous pouvez désabonner quelqu'un·e via la commande:
```
./notifyWebDiff unsubscribe -m <mail> -p <page> [-a]
```

### Ajouter / supprimer des administrateur·ices

La commande suivante vous permet d'ajouter un·e administrateur·ice à une page:
```
./notifyWebDiff manage -m <mail> -p <page>
```

et celle-ci vous permet de l'en supprimer:
```
./notifyWebDiff unmanage -m <mail> -p <page>
```

### Envoyer des commandes par mail

Si votre serveur de mail est correctement configuré, le programme vous offre la possibilité d'exécuter des commandes par mail.

La commande `pipe` s'occupe de lire le mail sur l'entrée standard, à la recherche du sujet du mail. La commande à exécuter, ainsi que ses paramètres, sont ensuite extraits.

Voici la liste des commandes possibles:
- `notify [<mail>] <page>`
- `subscribe [<mail>] <page>`
- `unsubscribe [<mail>] <page>`
- `manage <mail> <page>`
- `unmanage [<mail>] <page>`
- `disable <page>`
- `enable <page>`

Exemple: 
```
echo Subject: notify <page> | ./notifyWebDiff pipe --sender <mail> [-a]
```

Cette commande va notifier les abonné·es de la page donnée, si l'émetteur (`--sender`) est dans la liste des administrateur·ices de la page.

Si l'option `-a` est spécifiée, les administrateur·ices de la page recevront également la notification.

Exemple:
```
echo Subject: unsubscribe <mail> <page> | ./notifyWebDiff pipe --sender <mail>
```

Cette commande va désabonner l'adresse e-mail de la page donnée, si l'émetteur est administrateur·ice de la page ou si l'émetteur correspond à l'adresse e-mail à désabonner.
